import 'package:flutter/material.dart';
import 'package:prueba1/pantalla/home_screen.dart';

void main() {
  runApp(const MyApp()); //Carga la clase principal
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}
